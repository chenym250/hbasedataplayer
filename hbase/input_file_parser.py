import re
import os
import logging
import ConfigParser
import happybase
import time

logger = logging.getLogger(__name__)
config_dir = os.path.dirname(os.path.realpath(__file__)) + '/conf/inputFileParserConfig.ini'

class InputParser:

	config = ConfigParser.ConfigParser()
	batch = None
	currbatch = 0
	batchcount = 0

	def settable(self, info):
		hostname = self.readConfig('toHBase','hostname')
		tablename = self.readConfig('toHBase','tablename')
		tablename = self.toValue(tablename, info)
		connection = happybase.Connection(hostname)
		table = connection.table(tablename)
		self.batch = table.batch()

	def readConfig(self, module, key):
		if not self.config.sections():
			logger.info('read configurations')
			if os.path.isfile(config_dir):
				self.config.read(config_dir)
			else:
				logger.error('config file not found')
				logger.error('we are looking at %s', config_dir)
				return ''
		try:
			return self.config.get(module,key)
		except ConfigParser.NoSectionError or ConfigParser.NoOptionError:
			return ''

	def toRegex(self, keys, normalstr):
		for key in keys:
			normalstr = normalstr.replace('\"'+key+'\"', '(?P<'+key+'>\w+)')
		return normalstr


	def toValue(self, normalstr, keyvalues):
		p = re.compile(r'(\"\w+\")*')
		m = p.findall(normalstr)
		if m:
			for rawkey in m:
				key = rawkey[1:-1]
				if key in keyvalues:
					normalstr = normalstr.replace(rawkey, keyvalues[key])
		else:
			logger.debug('no key found in %s', normalstr)
		return normalstr

	def parse3lines(self, filename):
		self.parselines(filename, 0, True, 3);

	def parsealllines(self, filename):
		self.parselines(filename, 200, False, 0)

	def parselines(self, filename, batchsize, limit, maxcount):
		i = 0
		keys = self.readConfig('keys','key').split(',')
		titleinfo = {}

		t1 = time.time()

		for line in open('/home/cheny/qdaf/raw_data_for_testing/600101.t'):
			if limit and i >= maxcount:
				break
			if i == 0:
				tre = self.toRegex(keys, self.readConfig('parser','title'))
				tmatch = re.match(tre, line)
				if tmatch:
					titleinfo = tmatch.groupdict()
					self.settable(titleinfo)
				else:
					logger.error('bad regex, no match: %s', tre)
					logger.error(line)
					continue
				i = 1
				continue
			if i >= 2:
				rre = self.toRegex(keys, self.readConfig('parser','row'))
				rmatch = re.match(rre, line)
				if rmatch:
					rowdict = rmatch.groupdict()
					# concatenate two dicts to a new one
					kvs = titleinfo.copy()
					kvs.update(rowdict)
					self.hbaseinput(kvs)
				else:
					logger.error('bad regex, no match: %s', rre)
					logger.error(line)
					continue
			i += 1
			if (self.currbatch == batchsize):
				self.batch.send()
				self.currbatch = 0
				self.batchcount += 1

		if (self.currbatch != 0):
				self.batch.send()
				self.currbatch = 0

		t2 = time.time()

		logger.info('send %s lines and %s batches, took %s to finish', i, this.batchcount, t2-t1)


	def hbaseinput(self, keyvalues):
		cf = self.readConfig('toHBase','cf')
		cf = self.toValue(cf, keyvalues)
		col = self.readConfig('toHBase','col')
		col = self.toValue(col, keyvalues)
		rowkey = self.readConfig('toHBase','rowkey')
		rowkey = self.toValue(rowkey, keyvalues)
		if len(rowkey) < 18:
			rowkey = rowkey[0:9] + '0' + rowkey[9:]
		cell = self.readConfig('toHBase','cell')
		cell = self.toValue(cell, keyvalues)
		self.batch.put(rowkey, {cf+':'+col : cell})
		self.currbatch += 1