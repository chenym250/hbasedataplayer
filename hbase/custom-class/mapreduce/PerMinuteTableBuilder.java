package mapreduce;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.NavigableMap;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableOutputFormat;
import org.apache.hadoop.hbase.mapreduce.TableInputFormat;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.hbase.mapreduce.TableReducer;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;

/**
 * this class reads an hbase table, exacts per minute data and dump them
 * into either a file on hdfs or another table in hbase.
 */
public class PerMinuteTableBuilder {
	
	private static final String NAME = "PerMinuteTableBuilder";
	private static final int DEFAULT_PREFIX_LENGTH = 1;
	private static final int DEFAULT_PREFIX_OFFSET = 0;
	
	/**
	 * a custom mapper class. it attempts to find all of the LAST instances of data
	 * that stocks have at the end of the minutes. For this purpose, the class keeps
	 * two dictionaries as cache so we can always look back and fetch the last data
	 * we saw. 
	 */
	public static class MinuteStockMapper extends
	TableMapper<ImmutableBytesWritable, ImmutableBytesWritable> {

		private int prefixLength;
		private int prefixOffset;
		private byte[] familyName;
		private static final byte[] SEPERATOR = Bytes.toBytes("_");
		private static final int CHECKPOINT = 100;
		private int count;
		private Map<String, byte[]> prefixOfStocks = new HashMap<String, byte[]>();
		private Map<String, byte[]> valuesOfStocks = new HashMap<String, byte[]>();
		
		@Override
		protected void map(ImmutableBytesWritable rowKey, 
				Result result, Context context) 
				throws IOException, InterruptedException {
			
			// this should look like '20151202_0932' in String
			byte[] prefix = Bytes.copy(rowKey.get(), prefixOffset, prefixLength);
			NavigableMap<byte[], NavigableMap<byte[], byte[]>> wholeMap = result.getNoVersionMap();
			
			// assumes stock names are stored as column qualifiers
			NavigableMap<byte[], byte[]> stocks = wholeMap.get(familyName);
			if (stocks == null) {
				return;
			}
			
			// This loop iterates through the row. And by row I mean a collection
			// of key-value pairs with a common rowkey (in this case the rowkey
			// would be a per-second level timestamp). Each of the keys in these
			// keyvalue pairs is a unique stock name, and the values are the trade data. 
			// for the row keys, we would always use its prefix, which cuts the timestamp
			// to per-minute level.
			
			// the program keeps two cache for each of the stocks: a prefix that is last
			// seen with this stock, and the data that comes with them. the program always
			// updates the data's cache; but it only updates the prefix' cache when there's
			// a difference. Then, the program would know that the data we saw LAST time is 
			// the data we need, so it outputs that, and updates the prefix' cache.
			for (Map.Entry<byte[], byte[]> stockEntries : stocks.entrySet()) {
				
				byte[] stockNameInByte = stockEntries.getKey();
				String stockName = Bytes.toString(stockNameInByte);
				byte[] stockData = stockEntries.getValue();
				
				if (stockData == null) {
					continue;
				}
				// we always need to update the data, except when the data is null
				// and get the data at the same time
				byte[] prevData = valuesOfStocks.put(stockName, stockData);
				// previous prefix of this stock! we probably only needs to read it
				byte[] prevPrefix = prefixOfStocks.get(stockName);
				
				// the first cache
				if (prevPrefix == null) {
					prefixOfStocks.put(stockName, prefix);
				}
				  // this current prefix is a new one! meaning we are in a new time frame
			      // and the data we need to output is the previous one
				  else if (Bytes.compareTo(prefix, prevPrefix) != 0) {
					// form a new key, should look like 20121231_0913_600031
					byte[] rawKeyOfPrev = Bytes.add(prevPrefix, SEPERATOR, stockNameInByte);
					// send the data to the reducer
					context.write(new ImmutableBytesWritable(rawKeyOfPrev), 
							new ImmutableBytesWritable(prevData));
					prefixOfStocks.put(stockName, prefix);
				} // else we do nothing
			}
			// Set status every checkpoint lines
		    if(++count % CHECKPOINT == 0) {
		    	context.setStatus("Mapping row " + count + ": " + Bytes.toString(prefix));
		    }
		}

		@Override
		protected void setup(Context context) throws IOException,
		InterruptedException {
			Configuration configuration = context.getConfiguration();
			// here by default the length of the prefix is 0, meaning the mapper
			// won't emit anything to the reducer
			// this also assumes that all keys have same length, which is easy
			// to manage. 
		    prefixLength = configuration.getInt("minutestock.prefixLength", DEFAULT_PREFIX_LENGTH);
		    prefixOffset = configuration.getInt("minutestock.prefixOffset", DEFAULT_PREFIX_OFFSET);
		    familyName = Bytes.toBytes(configuration.get("minutestock.familyName"));
		    count = 0;
		} // this is method is called every time when map is called, so maybe we don't need too
		  // much going on here. 
	}
	
	/**
	 * a light-weighted custom reducer class. do what it does. 
	 */
	public static class AnalyzeReducer
	extends TableReducer<ImmutableBytesWritable, ImmutableBytesWritable, Put> {
		
		// these are hard-coded. but according to hbase's official reference guide, 
		// using constants improves the performance. 
		// @SEE http://hbase.apache.org/book.html#perf.general
		private static final byte[] CF = Bytes.toBytes("PM");
		private static final byte[] COL = Bytes.toBytes("json");
		
		@Override
		protected void reduce(ImmutableBytesWritable key, Iterable<ImmutableBytesWritable> values,
				Context context) throws IOException, InterruptedException {
			Put put = new Put(key.get()); // create a new put
			ImmutableBytesWritable result = null;
			for (ImmutableBytesWritable value:values) {
				result = value; // find the last one in the iterable, if there are multiple, 
			}
			put.addColumn(CF, COL, result.get());
			context.write(null, put);
		}
	}
	
	public static class AnalyzeReducerToFile
		extends Reducer<ImmutableBytesWritable, ImmutableBytesWritable, Text, Text> {
	@Override
		protected void reduce(ImmutableBytesWritable key, Iterable<ImmutableBytesWritable> values,
					Context context) throws IOException, InterruptedException {
			ImmutableBytesWritable result = null;
			for (ImmutableBytesWritable value:values) {
				result = value;
			}
			context.write(new Text(key.get()), new Text(result.get()));
		}
	}
	
	/**
	 * we can write the result from the mapper stage either to a file or to hbase. 
	 * this method gives the user the ability to choose between one of them. 
	 * @param description describes the reducer you want to choose
	 * @param dest the destination address where the reducer writes its outputs
	 * @param job the current job
	 * @return the updated job
	 * @throws IOException 
	 */
	public static Job setReducerType(String description, String dest, Job job) throws IOException {
		if (description.equals("toFile")) {
			job.setReducerClass(AnalyzeReducerToFile.class);
		    FileOutputFormat.setOutputPath(job, new Path(dest));
		} else if (description.equals("toHBase")) {
			TableMapReduceUtil.initTableReducerJob(
		    		  dest,
		    		  AnalyzeReducer.class,
		    		  job);
		    job.setOutputFormatClass(TableOutputFormat.class);
		} else {
			throw new IllegalArgumentException("the option is either \"toFile\" or \"toHBase\", and \"" 
					+ description + "\" is really not an option. ");
		}
		return job;
	}
	
	/**
	 * Job configuration.
	 */
	public static Job configureJob(Configuration conf, String [] args)
			throws IOException {
		String tableName = args[0];
		String familyName = args[1];
		int prefixLength = Integer.parseInt(args[2]);
		int prefixOffset = Integer.parseInt(args[3]);
		String reduceOption = args[4];
		String outputAddr = args[5];
		conf.setInt("minutestock.prefixLength", prefixLength);
		conf.setInt("minutestock.prefixOffset", prefixOffset);
		conf.set(TableInputFormat.INPUT_TABLE, tableName);
		conf.set("minutestock.familyName", familyName);
	    Job job = new Job(conf, NAME + "_" + tableName);
	    job.setJarByClass(PerMinuteTableBuilder.class);
	    Scan scan = new Scan();
	    scan.setCaching(500);
	    scan.setCacheBlocks(false);
	    TableMapReduceUtil.initTableMapperJob(
	    		  tableName,          // input table
	    		  scan,               // Scan instance to control CF and attribute selection
	    		  MinuteStockMapper.class,     // mapper class
	    		  ImmutableBytesWritable.class,         // mapper output key
	    		  ImmutableBytesWritable.class,  // mapper output value
	    		  job);
	    job = setReducerType(reduceOption, outputAddr, job);
	    job.setNumReduceTasks(1);
	    TableMapReduceUtil.addDependencyJars(job);
	    return job;
	  }
	
	public static void main(String[] args) throws Exception {
		Configuration conf = HBaseConfiguration.create();
		System.out.println(Arrays.toString(args));
		if (args.length != 6) {
			System.err.println("Only " + args.length + " arguments supplied, required: 6");
		    System.err.println("Usage: PerMinuteTableBuilder <TABLE_NAME> <COLUMN_FAMILY_NAME> <PREFIX_LENGTH> <PREFIX_OFFSET> [toFile/toHBase] <FILE_DIR/TABLE_NAME>");
		    System.exit(-1);
		}
		Job job = configureJob(conf, args);
	    System.exit(job.waitForCompletion(true) ? 0 : 1);
	}
}
