package mapreduce;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;

public class ImmutableBytesWritableWithKey extends ImmutableBytesWritable {
	
	private byte[] key;
	private int keyLength;
	
	/**
	 * empty constructor. for serialization and .etc
	 */
	public ImmutableBytesWritableWithKey() {
		super();
	}
	
	public ImmutableBytesWritableWithKey(byte[] key, byte[] bytes) {
		super(bytes);
		this.key = key;
		this.keyLength = key.length;
	}
	
	public byte[] getKey() {
		return key;
	}
	
	public void setKey(byte[] key) {
		this.key = key;
	}
	
	@Override
	public void readFields(final DataInput in) throws IOException {
	    super.readFields(in);
	    this.keyLength = in.readInt();
	    this.key = new byte[this.keyLength];
	    in.readFully(this.key);
	}
	
	@Override
	public void write(final DataOutput out) throws IOException {
	    super.write(out);
	    out.writeInt(this.keyLength);
	    out.write(this.key);
	}
	
	/**
	 * code copied from {@link org.apache.hadoop.hbase.io.ImmutableBytesWritable#toString()}
	 */
	public String toString() {
		StringBuilder sb = new StringBuilder(3*this.keyLength);
	    final int endIdx = this.keyLength;
	    for (int idx = 0; idx < endIdx ; idx++) {
	    	sb.append(' ');
	    	String num = Integer.toHexString(0xff & this.key[idx]);
	    	// if it is only one digit, add a leading 0.
	    	if (num.length() < 2) {
	    		sb.append('0');
	    	}
	    	sb.append(num);
	    }
	    String keyString = sb.length() > 0 ? sb.substring(1) : "";
		return "key: " + keyString + " value: " + super.toString();
	}
	
	public int compareTo(ImmutableBytesWritableWithKey that) {
		int compareKey = Bytes.compareTo(this.key, that.key);
		if (compareKey == 0) {
			return super.compareTo(that.get());
		}
		return compareKey;
	}

	/**
	 * when compare with the parent class
	 */
	@Override
	public int compareTo(ImmutableBytesWritable that) {
		if (that instanceof ImmutableBytesWritableWithKey) {
			return compareTo((ImmutableBytesWritableWithKey) that);
		} else {
			return compareTo(that.get());
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof ImmutableBytesWritableWithKey) {
			return compareTo((ImmutableBytesWritableWithKey)obj) == 0;
		} else {
			return super.equals(obj);
		}		
	}
	
	
}
