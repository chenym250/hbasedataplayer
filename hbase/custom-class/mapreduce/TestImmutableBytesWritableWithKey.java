/**
 * 
 */
package mapreduce;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.Test;

import junit.framework.TestCase;

public class TestImmutableBytesWritableWithKey extends TestCase {

	@Test
	public void testConstructor() {
		
		byte[] value = Bytes.toBytes("value");
		byte[] key = Bytes.toBytes("key");
		
		ImmutableBytesWritableWithKey ibwwk = new ImmutableBytesWritableWithKey(key, value);
		// the values and the keys are stored as reference
		assertTrue(value == ibwwk.get());
		assertTrue(key == ibwwk.getKey());
		System.out.println(ibwwk);
	}
	
	@Test
	public void testSort() {
		
		// test sorting of ImmutableBytesWritable,
		// and show later that ImmutableBytesWritableWithKey will do differently
		byte[] valueSmall = Bytes.toBytes("a value");
		byte[] valueLarge = Bytes.toBytes("a value that's bigger");
		ImmutableBytesWritable ibwSmall = new ImmutableBytesWritable(valueSmall);
		ImmutableBytesWritable ibwLarge = new ImmutableBytesWritable(valueLarge);
		List<ImmutableBytesWritable> list = new ArrayList<ImmutableBytesWritable>();
		list.add(ibwLarge);
		list.add(ibwSmall);
		String listOrder1 = list.toString();
		System.out.println(listOrder1);
		Collections.sort(list);
		String listOrder2 = list.toString();
		System.out.println(listOrder2);
		// make sure the list has change its sequence
		assertFalse(listOrder1.equals(listOrder2));
		ImmutableBytesWritable result1 = list.get(0);
		// make sure that ImmutableBytesWritable with valueSmall is come out first
		assertTrue(result1 == ibwSmall);
		// we are done with ImmutableBytesWritable
		
		byte[] keySmall = Bytes.toBytes("1234");
		byte[] keyLarge = Bytes.toBytes("5678");
		// small key with large value, large key with large value,
		// this should flip the result as it is in the case with ImmutableBytesWritable
		ImmutableBytesWritableWithKey ibwwkSmall= 
				new ImmutableBytesWritableWithKey(keySmall, valueLarge);
		ImmutableBytesWritableWithKey ibwwkLarge= 
				new ImmutableBytesWritableWithKey(keyLarge, valueSmall);
		List<ImmutableBytesWritable> list2 = new ArrayList<ImmutableBytesWritable>();
		list2.add(ibwwkLarge);
		list2.add(ibwwkSmall);
		String listOrder3 = list2.toString();
		// a correct sort will reverse the order between large and small
		Collections.sort(list2);
		String listOrder4 = list2.toString();
		System.out.println(listOrder3 + "\n" + listOrder4);
		// the order has changed
		assertFalse(listOrder3.equals(listOrder4));
		// we get the small out first even we put it in second
		assertTrue(ibwwkSmall == list2.get(0));
	}
	
	@Test
	public void testSerialize() throws IOException {
		
		byte[] value = Bytes.toBytes("value");
		byte[] key = Bytes.toBytes("key");
		
		ImmutableBytesWritableWithKey ibwwk = new ImmutableBytesWritableWithKey(key, value);
		ImmutableBytesWritableWithKey ibwwk2 = new ImmutableBytesWritableWithKey();
		
		try {
			ibwwk.equals(ibwwk2);
			fail();
		} catch (NullPointerException e) {
			// as we thought, do nothing
		}
		
		FileOutputStream fos=new FileOutputStream("test.data");  
        DataOutputStream dos=new DataOutputStream(fos);  
        ibwwk.write(dos);
        dos.close(); 
  
        FileInputStream fis=new FileInputStream("test.data");  
        DataInputStream dis=new DataInputStream(fis);
        ibwwk2.readFields(dis);
        // they are not the same object
        assertFalse(ibwwk == ibwwk2);
        // but they are equal
        assertEquals(ibwwk, ibwwk2);
        assertEquals(ibwwk.toString(), ibwwk2.toString());
        dis.close();
		
	}
	
}
