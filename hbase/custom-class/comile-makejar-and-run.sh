#!/bin/bash

function compile
{
    #compile
    echo "to compile"
    javac -cp `hbase classpath`:$pwd $dir/$filename.java
    echo "$filename is compiled"
}

function makejar
{
    #run
    echo "make a jar"
    jar cf $filenameInJar.jar $dir/
    echo "$filename is now a jar"
}

function run
{
    #run
    echo "to run"
    HADOOP_CLASSPATH=`hbase classpath` hadoop jar $filenameInJar.jar $dir/$filename $comm
    echo "$filename is completed"
}

dir=$1
shift
filename=$1
shift
comm=$@

read -n1 -p "compile? [y,n]" doit 
echo ""
case $doit in  
  y|Y) compile ;;
  n|N) echo "no compile" ;; 
  *) echo "unknown command" ;; 
esac

read -n1 -p "make a jar? [y,n]" doit 
echo ""
case $doit in  
  y|Y) makejar ;;
  n|N) echo "no make jar" ;; 
  *) echo "unknown command" ;; 
esac

read -n1 -p "run? [y,n]" doit 
echo ""
case $doit in  
  y|Y) run ;;
  n|N) echo "no run" ;; 
  *) echo "unknown command" ;; 
esac