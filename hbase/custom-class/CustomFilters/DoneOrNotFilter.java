package CustomFilters;

import org.apache.hadoop.hbase.filter.FilterBase;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.exceptions.DeserializationException;

public class DoneOrNotFilter extends FilterBase {

	private int done;

	public DoneOrNotFilter(int done) {
		this.done = done;
	}

	@Override
 	public ReturnCode filterKeyValue(Cell v) {
		return ReturnCode.INCLUDE;
 	}

	// Override here explicitly as the method in super class FilterBase might do a KeyValue recreate.
	// See HBASE-12068
	@Override
	public Cell transformCell(Cell v) {
		return v;
	}
	
	// public boolean filterRow() {
	// 	return (done > 10);
	// }

	public boolean filterAllRemaining() {
		return (done > 10);
	}

	/**
	* @return The filter serialized using pb
	*/
	public byte [] toByteArray() {
		byte[] toSerialBtArr = new byte[1];
		toSerialBtArr[0] = new Integer(done).byteValue();
		return toSerialBtArr;
	}

	/**
	* @param pbBytes A pb serialized {@link PageFilter} instance
	* @return An instance of {@link PageFilter} made from <code>bytes</code>
	* @throws DeserializationException
	* @see #toByteArray
	*/
	public static DoneOrNotFilter parseFrom(final byte [] pbBytes)
	throws DeserializationException {
	
		int returnDone = new Byte(pbBytes[0]).intValue();
		return new DoneOrNotFilter(returnDone);
	}
}
