package CustomFilters;

import org.apache.hadoop.hbase.filter.FilterBase;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.exceptions.DeserializationException;
import org.apache.hadoop.hbase.util.Bytes;

import java.util.Arrays;

/**
* a custom filter. It filters rows based on their prefix. 
* Only if the prefix of a row is brandly new to the filter,
* shall the row be passed to client. In another word, it
* reads all rows from a table and filters out every row with
* a row key which it has seen before. 
*/
/**
* reference: PageFilter, PrefixFilter, FuzzyRowFilter 
*/
public class PrefixSkippingFilter extends FilterBase {

	protected int prefixLength = 1;
	protected byte[] prefix;
	protected boolean filterRow;

	/**
	 * @TODO: add some condition check, or make sure
	 * the way we convert int to byte[] is safe for
	 * all int. but more importantly, we need to make
	 * sure this value is not going to exceed the range
	 * of a rowkey
	 */
	public PrefixSkippingFilter(final int prefixLength) {
		this.prefixLength = prefixLength;
		this.prefix = null;
	}

	public int getPrefixLength() {
		return this.prefixLength;
	}

	/**
	* extended from PrefixFilter. Compare a locally stored prefix with a prefix
	* the server gives us.
	* HBase does not mention this anywhere in there documentation, but I believed
	* the parameters work like this: 
	*   buffer = [\x00, \x01, \x02, \x03, \xA2, \xFF, \x65, \x69]
	*                        |← prefix = 0x0203A2FF →|          ↳ lenngth = 8
	*            offset = 2 ↗             ↳ prefix.lenngth = 4
	*
	*/
	public boolean filterRowKey(byte[] buffer, int offset, int length) {
		if (buffer == null || length < this.prefixLength) {
			return true;
		}
		boolean newPrefix = (this.prefix == null);
		// if they are equal, return true => filter row
		// else return false, pass row
		// if we are passed the prefix, set flag
		if (!newPrefix) {
			int cmp = Bytes.compareTo(buffer, offset, this.prefixLength, this.prefix, 0,
				this.prefixLength);
			newPrefix = (cmp != 0);
		}
		// if ((!isReversed() && cmp > 0) || (isReversed() && cmp < 0)) {
		// 	passedPrefix = true;
		// }
		this.prefix = Bytes.copy(buffer, offset, this.prefixLength);
		filterRow = !newPrefix;
		return filterRow;
	}

	@Override
	public ReturnCode filterKeyValue(Cell v) {
		if (filterRow) return ReturnCode.NEXT_ROW;
		return ReturnCode.INCLUDE;
	}

	// Override here explicitly as the method in super class FilterBase might do a KeyValue recreate.
	// See HBASE-12068
	@Override
	public Cell transformCell(Cell v) {
		return v;
	}

	/**
	* Now this is a dumb: since the only parameter we have now is an integer value
	* that will likely to be small (how long do you want your row key to be anyway?)
	* this method will pack that integer as a byte array and sends it.
	* @TODO: instead of put this integer as a single byte (max: 127), maybe put it
	* as an actual byte array with a dynamical length.
	* @TODO: if we wish to use protocol buffer, maybe we can extend PrefixFilter class?
	* @return The filter serialized using pb
	*/
	public byte [] toByteArray() {
		byte[] toSerialBtArr = new byte[1];
		toSerialBtArr[0] = new Integer(this.prefixLength).byteValue();
		return toSerialBtArr;
	}

	/**
	* @param pbBytes A pb serialized {@link PrefixSkippingFilter} instance
	* @return An instance of {@link PrefixSkippingFilter} made from <code>bytes</code>
	* @throws DeserializationException
	* @see #toByteArray
	*/
	public static PrefixSkippingFilter parseFrom(final byte [] pbBytes)
	throws DeserializationException {
	
		int returnPrefixLength = new Byte(pbBytes[0]).intValue();
		return new PrefixSkippingFilter(returnPrefixLength);
	}

	// /**
	// * @param other
	// * @return true if and only if the fields of the filter that are serialized
	// * are equal to the corresponding fields in other.  Used for testing.
	// */
	// boolean areSerializedFieldsEqual(Filter o) {
	// 	if (o == this) return true;
	// 	if (!(o instanceof PrefixSkippingFilter)) return false;

	// 	PrefixSkippingFilter other = (PrefixSkippingFilter) o;
	// 	return other.getPrefixLength() == o.getPrefixLength();
	// }

}
