#QDAF数据播放器#

##0. Introduction##
一个简单的数据播放器，尝试模拟一个完整的数据读取流程。目前仅支持HBase数据库。

##1. environment setup##

###1.1. HBase environment setup###
（在已经成功搭建了HBase的前提下）
为了最大化HBase的使用效率，我们需要在HBase原生的功能基础上添加custom classes。添加方法如下：

1. 打包 HbaseDataPlayer/hbase/custom classes。例：运行 /make-jar.sh 可打包 custom-filter 包，依此修改可打包其他 package
2. 修改 HBase 目录下的 conf/hbase-env.sh，添加 `export HBASE_CLASSPATH=（your repo dir）/hbase/custom-class/CustomClasses.jar`
3. 重新启动HBase。 列：运行 hbase/restart-hbase.sh

目前QDAF数据播放器仅使用了mapreduce.ImmutableBytesWritableWithKey，但在未来可能会使用 endpoint 和 custom filter。

reference：官方指南custom filter章节

###1.2. HBase table setup###
通过hbase shell创造table。QDAF数据播放器里所有涉及到table 操作的部分都假设table已经存在。

一致性：

导入：通过修改config file保持一致性。参考：readme under qdafDataPlayer/hbase

导出：通过运行时添加的parameter

需要注意的是，在把筛选后的原始数据导入到分钟级数据的表格之前，请确保family column

###1.3. eclipse setup###
建议使用eclipse搭建 Java 环境。

如果使用eclipse需要添加的path：(your local hbase dir)/lib目录下的所有jar包，custom classes.jar
（不建议将HbaseDataPlayer/hbase与src放在同一个project里。应当把HbaseDataPlayer/hbase与HBase连在一起，再把HBase添加到project path里），
rabbitmq 和jackson（eclipse应当能自动完成添加）。

如果不使用eclipse而直接使用Java终端时需要添加的java -cp：\`hbase classpath\`:$pwd:{your rabbitmq bins}:{jackson package}

##2. package contents##

###2.1. hbase###
主要模拟数据库管理员需要进行的操作。运行main.py导入原始数据，运行/custom-class/comile-makejar-and-run.sh 从已有的数据表中提取分钟级数据并导出至另一张HBase表/hdfs文件。

更多内容请参见hbase 文件夹下的readme

###2.2. src###
    historicalData
        ├── ConfigParser.java  ----ReadCreator通过ConfigParser阅读reader的参数
        ├── HBaseFormatUtils.java ----提供 java.Date 和 hbase 所使用的 byte[] 之间的转换
        ├── ReadCreator.java ----client通过ReadCreator，以StockData的形式获取信息
        ├── readers
        │   ├── DataReader.java ----接口
        │   ├── HBaseExhaustivePerMinuteTableReader.java ----连续读取
        │   ├── HBaseExhaustiveReader.java ----连续读取
        │   ├── HBaseReader.java ----抽象，继承DataReader
        │   └── HBaseSkippingFilterReader.java ----unimplemented
        └── StockData.java ----包装数据（数据的格式为String）

###2.3. end-user-simulator###
SimpleClient

永远监听一个mq。当收到指令时，创造一个新的线程；新线程通过DataReader读取数据，并通过mq将数据传送到另一条mq

##3. run##

###3.1. 导入###
1. 配置

    打开 HbaseDataPlayer/hbase/conf/inputFileParserConfig.ini. 这个文件尝试用regex来读取原始数据，但目前的速度并不理想。修改 `[toHBase]` 指定表格和列族。
    
2. 目前的表格设计
    
    timestamp 作为 rowkey（添加0使得长度统一化），stock name 作为 column，单一列族，每支股票每一刻的所有数据以字符串的形式占据一个单元。暂时不建议修改schema，因为
    在创造分钟级数据时这种schema是被默认的并且不能在runtime修改的。
    
3. 运行
    
    修改 HbaseDataPlayer/hbase/main.py，确定 原始文件路径和相应的 config file。保存后运行文件。
    
4. 通过hbase shell检查一下结果

###3.2. 创造分钟级的表格###
1. 配置

    确保目标表格已经建立，并且有名为PM的列族
    
    确保custom classes已经添加到了hbase server的build path中 （echo \`hbase classpath\`）
    
2. 运行 HbaseDataPlayer/hbase/custom-class/comile-makejar-and-run.sh，格式如下
    `$./comile-makejar-and-run.sh {package name} {main class name} {your preferred jar name, without extension} {source table} {source cf} {prefix length} {prefix offset} [toFile/toHBase] {file dir/table name}`
    
    其中`prefix length`和`prefix offset`决定如何从rowkey中提取分钟的数据
    （设 rowkey = abc20130212_09002555555_def 代表2013年2月12号九点零25秒55...，则prefix offset 应为3，prefix length应为13，得到的prefix应为20130212_0900。此处要求
    排序后的rowkey在分钟内是按时间顺序排列的）
    
    需要从`[toFile/toHBase]`中选择一个。 `toFile`会将结果写到指定的hdfs目录中，`toHBase`会将数据写到指定的hbase表格中。注意此处需完整打出`toFile`或者`toHBase`，不能有其他选项
    
     `{file dir/table name}`：如果之前选择`toFile`，此处填写hdfs路径；如果之前选择`toHBase`，此处填写hbase table name。
     注意如果是`toFile`，则hdfs路径**不能**被提前创建，必须为空；而如果是`toHBase`，则table必须**已经**被创建，必须存在，且列族名为PM
    
    请确定hbase正在运行中。
    
3. 通过hdfs dfs 或者hbase shell检查结果

###3.3. 通过mq读取数据###
1. 运行client
    通过eclipse 或者terminal运行，class path的添加如前所述。
    
2. 通过mq传送指令
    指令queue的名称为InputMQ2
    
    对sender没有特殊的要求，rabbitmq第一个tutorial的就行。
    
    指令格式为json，如：`{"stocks":["600036"],"start":"2013/12/04-09:00:00000","stop":"2013/12/05-09:00:00000"，"step":"minute"}` 
    可选择的选项有`stocks`（默认全部），`step`（默认分钟级），`start`（默认第一行数据） 和 `stop`（默认最后一行数据）。未明确的选项和格式错误的选项都会依照默认处理。
    
    运行sender发送数据
    
3. 通过mq接受数据
    数据queue的名称为OutputMQ
    
    对receiver没有特别的要求，rabbitmq第一个tutorial的就行。

附：目前使用的sender和receiver

sender:

    #!/usr/bin/env python
    import pika
    
    connection = pika.BlockingConnection(pika.ConnectionParameters(
            host='localhost'))
    channel = connection.channel()
    
    channel.queue_declare(queue='InputMQ2')
    
    channel.basic_publish(exchange='',
                          routing_key='InputMQ2',
                          body='{"stocks":["600036"],"start":"2013/12/04-09:00:00000","stop":"2013/12/05-09:00:00000"}')
    connection.close()
    
receiver:

    #!/usr/bin/env python
    import pika
    
    connection = pika.BlockingConnection(pika.ConnectionParameters(
            host='localhost'))
    channel = connection.channel()
    
    channel.queue_declare(queue='OutputMQ')
    
    print ' [*] Waiting for messages. To exit press CTRL+C'
    
    def callback(ch, method, properties, body):
        print " [x] Received %r" % (body,)

    channel.basic_consume(callback,
                          queue='OutputMQ',
                          no_ack=True)
    
    channel.start_consuming()
    
##4. Leftover issues##

这里是现有的bug和未能妥善处理的地方

###4.1. general###
没有 API，没有 unit test

###4.2. 导入###
1. 使用 regex 不如 hard-coded 的速度快：速度相差约50%，应考虑简化regex的复杂度

2. rowkey 长度统一化的步骤目前是 hard-coded

3. config file 里引号的使用很受限制，而通过阅读config file达到的功能也不够灵活

    方案：增加functiion功能，可以在每一次阅读属性后对属性运行指定的method。因为python的灵活特性，这点不难实现

4. config file 和 source file 的指定可以更加直接，例如通过command line 或者 GUI。API还需更加完善。

5. 目前很多创建和检查的工作需要通过 hbase shell 来完成，其实这部分也有 java API 可调用

6. 没有测试硬盘存储

###4.3. 创造分钟级的表格###
1. 仍有很大一部分是hard-coded，没有API

2. command line 过于复杂，应考虑通过Java 或者 python 运行command line，并在此之上建立API

3. 压力测试做的不够。目前只在总计60M的原始数据上进行过测试（mapreducde需要较长的准备时间，在15秒以上，而实际运行常为数秒）

4. 在此基础上，效率测试做的不够。没有测试运行时间与数据量之间的线性/非线性关系，没有测试region split的影响。目前的模型是在region split需要特殊处理的假设下进行的（既一分钟的数据可能分落两个区间，被两个mapper分开独立执行）
    而如果证明分区并不会影响结果的话，则可以摒弃reducer，提高效率
    
5. 没有在真正的分机模式下测试过。

6. 在只需要读取少量数据时，因为延迟时间而显得反应太慢
    
    方案：使用 coprocessor 和 endpoint。值得注意的是，在 HBase 0.98+ 使用 custom filter 和 endpoint 需要调用 Google protobuf，而官方指南和网上大部分的教程的版本相对落后，没有涉及到protocol buffer，下个版本的API可能还会有变动，因此可能会对开发造成影响。
    
    
    参见：http://blog.csdn.net/yangzongzhao/article/details/24306775
    
7. 单独建立新表进一步消耗了存储空间

    方案：使用endpoint，或者直接把mapreduce的结果输出到mq
    
###4.4. 运行client###
1. readCreator 目前通过读取config file 来选择reader，但事实上这种方式十分促狭，扩展性差，对于复杂的逻辑并不合适。使用SQL数据库可能会好些

2. Reader和client 不支持多线程，因此在全部数据读完之前程序都处于假死状态
    
    方案：在reader添加新的method，可以在数据尚未读完时就返回存放数据的 (thread-safe) collector；修改client，当获得任务时创造两个新线程，一个数据读取线程，一个数据传输线程，二者共享reader 的 collector，只要 collector 内有新数据就可以通过mq向外传输。

3. class和package可能需要重新组合。

4. exception handling 相当粗糙

5. StockData 的转换效率没有被测试，使用String储存数据可能并非最佳方案
    
    方案：使用JsonObject？但对数据的解析是否应该在远程的用户那里进行？

###4.5. 通过mq的传送和接收###
1. mq传送数据效率很差

    方案：在client的loop里增加sleep？
    
2. 在远端的指令发送和接收代码之上应该有API或者GUI

3. client端对传输速度没有控制

4. 应该为client编写抽象类或者接口，因为接收mq的client也可能是多样的。

##5. 下一步计划##

1. 建立 unit test，和支持批量、大量运行的流程测试（既对比导入的原始数据与最后导出的数据，测试运行时间等）

2. 添加第二种数据库（mongodb），丰富选择逻辑（最好包括随机选择），对比与hbase的效率

3. 尝试使用Maven，Hive等辅助HBase的工具

4. 使用HBase的endpoint等功能