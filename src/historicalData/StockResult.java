package historicalData;

import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;

/**
 * This class is designed as a generic data holder that clients
 * can use across database. Clients shall be able to extract
 * the stock data from different databases with ease, and only
 * needs to deal with jva's default data types. It is the job of
 * the readers to convert and parse some of the data. 
 * The data format can be seen below:
 *   Date time: the timestamp of a trade, in java.util.Date
 *   String stock: the name or the symbol of a stock
 *   Map<String, Long> data: the data
 *     a potential problem: we are forcing the readers to parse
 *     every single kv-pair they read. this is a lot of calculations
 *     and might hurt the performance. 
 */
public class StockResult implements Comparable<StockResult> {

	// the timestamp of a data
	private Date time;
	// the name/symbol of a stock
	private String stock;
	// data, key is the headers (i.e. ask1, bid5)
	private Map<String, Long> data;
	// the key to sort, by date or by stock
	private List<String> sortKey;
	
	public StockResult() {
		this.sortKey = new ArrayList<String>();
	}
	
	public StockResult(Date time, String stock, 
			Map<String, Long> data) {
		this(time, stock, data, new ArrayList<String>());
	}
	
	public StockResult(Date time, String stock, 
			Map<String, Long> data, List<String> sortKey) {
		this.time = time;
		this.stock = stock;
		this.data = data;
		this.sortKey = sortKey;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public String getStock() {
		return stock;
	}

	public void setStock(String stock) {
		this.stock = stock;
	}

	public Map<String, Long> getData() {
		return data;
	}

	public void setData(Map<String, Long> data) {
		this.data = data;
	}
	
	public void setSortKey(List<String> sortKey) {
		this.sortKey = sortKey;
	}
	
	public List<String> getSortKey() {
		return sortKey;
	}
	
	public void setTimeAsKey() {
		this.sortKey.add("time");
	}
	
	public void setStockAsKey() {
		this.sortKey.add("stock");
	}

	@Override
	public int compareTo(StockResult s0) {
		if (sortKey == null) {
			return 0;
		}
		for (String key:sortKey) {
			int comp = compareTo(s0, key);
			if (comp != 0) {
				return comp;
			}
		}
		return 0;
	}
	
	private int compareTo(StockResult s0, String key) {
		if (key.equals("time")) {
			return this.time.compareTo(s0.time);
		} else if (key.equals("stock")) {
			return this.stock.compareTo(s0.stock);
		}
		return 0;
	}
	
	
}
