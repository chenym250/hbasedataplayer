/**
 * 
 */
package historicalData.readers;

import historicalData.StockResult;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.hbase.client.HTable;

/**
 * This reader shall be used merely for testing. It returns
 * every single row the database scans, which creates too many
 * RPC and may consume too many resources. 
 */
public class HBaseExhaustiveReader extends HBaseReader {

	/* (non-Javadoc)
	 * @see historicalData.readers.Reader#fetchOne()
	 */
	@Override
	public StockResult fetchOne() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see historicalData.readers.Reader#fetchAll()
	 */
	@Override
	public List<StockResult> fetchAll() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see historicalData.readers.HBaseReader#prepareHBase(org.apache.hadoop.hbase.client.HTable)
	 */
	@Override
	public void prepareHBase(HTable table) throws IOException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see historicalData.readers.HBaseReader#setConfiguration(byte[], byte[], int, int)
	 */
	@Override
	public void setConfiguration(byte[] startTime, byte[] endTime, int maxRead,
			int maxCache) throws IOException {
		// TODO Auto-generated method stub

	}

}
