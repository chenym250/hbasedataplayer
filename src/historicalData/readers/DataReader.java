package historicalData.readers;

import historicalData.StockResult;

import java.util.List;
import java.util.Date;
import java.io.IOException;

public interface DataReader {

	public void prepare(String tableName) throws IOException;
	public StockResult fetchOne() throws IOException;
	public List<StockResult> fetchAll() throws IOException;
	public void prepare(String tableName, Date startTime, Date endTime, int maxRead, int maxCache) throws IOException;
}