package historicalData.readers;

import historicalData.StockResult;
import historicalData.HBaseRowKeyTimeUtils;

import java.util.Date;
import java.util.Map;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Result;

public abstract class HBaseReader implements DataReader {

	public abstract void prepareHBase(HTable table) throws IOException;
	public abstract void setConfiguration(byte[] startTime, byte[] endTime, int maxRead, int maxCache) throws IOException;

	/**
	 * @TODO: figure out what's supposed to do with the deprecated API
	 */
	public void prepare(String tableName) throws IOException {
		Configuration conf = HBaseConfiguration.create();
		HTable table = new HTable(conf, "md2");
		prepareHBase(table);
	}
	
	public void prepare(String tableName, Date startTime, 
			Date endTime, int maxRead, int maxCache)
					throws IOException{
		setConfiguration(HBaseRowKeyTimeUtils.toRowKey(startTime), 
				HBaseRowKeyTimeUtils.toRowKey(endTime), 
				maxRead, maxCache);
		prepare(tableName);
	}
	
	public static StockResult covertResult(Result result) {
		Date time = HBaseRowKeyTimeUtils.toDate(result.getRow());
		String stock = "";
//		Map<byte[], byte[]> cellMap = result.getFamilyMap(HBaseRowKeyTimeUtils.toRowKey(""));
		
		Map<String, Long> data = null;
		return new StockResult(time, stock, data);
	}
}