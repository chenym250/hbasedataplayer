package historicalData.readers;

import historicalData.HBaseRowKeyTimeUtils;
import historicalData.StockResult;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.PageFilter;
import org.apache.hadoop.hbase.client.ResultScanner;
import org.apache.hadoop.hbase.client.Result;

import CustomFilters.PrefixSkippingFilter;

public class HBaseSkippingFilterReader extends HBaseReader {
	
	private static final int DEFAULT_MAXCACHE = 10000;
	
	private Scan scan;
    private ResultScanner scanner;
    private int maxCache;
    private int maxRead;
    private int localCount;
    private boolean done;

    public HBaseSkippingFilterReader() {
        this.scan = new Scan();
        this.scanner = null;
        this.maxCache = DEFAULT_MAXCACHE;
        this.done = false;
    }
	
	@Override
	public StockResult fetchOne() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<StockResult> fetchAll() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void prepareHBase(HTable table) throws IOException {
		Filter filter = createSkippingFilter();
        scan.setFilter(filter);
        // PrefixSkippingFilter can only scan the first
        // prefixes, so if we wish to obtain the last ones,
        // we have to scan backward. 
        scan.setReversed(true);
		scanner = table.getScanner(scan);
	}

	@Override
	public void setConfiguration(byte[] startTime, byte[] endTime, int maxRead,
			int maxCache) throws IOException {
		scan.setStopRow(startTime);
        scan.setStartRow(endTime);
		this.maxCache = maxCache;
		this.maxRead = maxRead;
	}
	
	private Filter createSkippingFilter() {
		return new PrefixSkippingFilter(HBaseRowKeyTimeUtils.getPrefixFormat().length());
	}
	
	private Filter createPageFilter() {
		return new PrefixSkippingFilter(HBaseRowKeyTimeUtils.getPrefixFormat().length());
	}
	
}