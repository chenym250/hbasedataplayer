package historicalData;

import historicalData.readers.DataReader;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;
import java.util.Map;
import java.util.Properties;

public class ConfigParser {

	private Properties p;
	private String dir;
	public static final String DEFAULT_CONF_FOLDER = "conf";
	public static final String CONFIG_NOT_FOUND = "notFound";
	
	public ConfigParser() {
		this(DEFAULT_CONF_FOLDER);
	}
	
	public ConfigParser(String dir) {
		p = new Properties();
		this.dir = dir;
	}
	
	public DataReader getThisReader(String dbName, int step) {
		String stepKey;
		if (step == ReadCreator.PER_SECOND) {
			stepKey = "skim";
		} else if (step == ReadCreator.PER_SECOND) {
			stepKey = "batch";
		} else {
			stepKey = "default";
		}
		String readerKey = dbName + "." + stepKey + ".readers";
		try {
			return (DataReader) Class.forName(recursiveKeySearch(readerKey)).newInstance();
		} catch (InstantiationException | IllegalAccessException
				| ClassNotFoundException e) {
			e.printStackTrace();
			throw new RuntimeException("cannot create a reader because "
					+ readerKey + " cannot be initialized.");
		} catch (NullPointerException e) {
			e.printStackTrace();
			throw new RuntimeException("cannot find any value associated with key " 
			+ readerKey);
		}
	}
	
	public String getTableName(Date start, Date stop, String stock) {
		return p.getProperty("database.base");
	}
	
	public void readSelectionConf() throws IOException {
		loadProperties("database_selection.properties");
		
	}
	
	private void loadProperties(String fileName) throws IOException {
		File f = new File(dir, fileName);
		try {
			p.load(new FileInputStream(f));
		} catch (FileNotFoundException e) {
			System.err.println("illegal path: " + f.getAbsolutePath());
			throw(e);
		}
	}
	
	// this method works when each key pairs with one and only one value
	private String recursiveKeySearch(String key) throws NullPointerException {
		String returnKey = p.getProperty(key);
		if (returnKey == null) {
			throw new NullPointerException(
					"cannot find any value associated with key " + key);
		}
		String prefix = returnKey.substring(0, 2);
		if (prefix.equals("./")) {
			return recursiveKeySearch(returnKey.substring(2));
		} else if (prefix.equals("~/")) {
			return recursiveKeySearch(key + "." + returnKey.substring(2));
		} else {
			return returnKey;
		}
	}
	
	// this one might work for multiple values, but it seems pointless to
	// give a key and get all other key value pairs it can reach for. 
	// The performance AND the correctness of the recursion has NOT been tested. 
	private Map<String, String> recursiveMutipleKeySearch(
			String key, Map<String, String> keyMap) throws NullPointerException {
		String returnKeys = p.getProperty(key);
		for (String returnKey:returnKeys.split(",")) {
			if (keyMap.containsKey(key)) {
				continue;
			}
			String prefix = returnKey.substring(0, 2);
			if (prefix.equals("./")) {
				recursiveMutipleKeySearch(returnKey.substring(2), keyMap);
			} else if (prefix.equals("~/")) {
				recursiveMutipleKeySearch(key + "." + returnKey.substring(2), keyMap);
			} else {
				keyMap.put(key, returnKey);
			}
		}
		return keyMap;
	}
	
}
