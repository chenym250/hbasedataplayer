package historicalData;

import java.util.Date;
import java.util.Calendar;
import java.util.Map;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.io.IOException;

import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.util.Bytes;

public class HBaseFormatUtils {

	private static final String DEFAULT_ROW_FORMAT = "yyyyMMdd_HHmmssSSS";
	private static final String DEFAULT_PREFIX_FORMAT = "yyyyMMdd_HHmm";

	private static String fullRowFormat = DEFAULT_ROW_FORMAT;
	private static String prefixFormat = DEFAULT_PREFIX_FORMAT;
	private static SimpleDateFormat rowFormatter = new SimpleDateFormat(fullRowFormat);
	private static SimpleDateFormat prefixFormatter = new SimpleDateFormat(prefixFormat);
	
	
	public static String getFullRowFormat() {
		return fullRowFormat;
	}

	public static String getPrefixFormat() {
		return prefixFormat;
	}
	
	public static byte[] toRowKey(String timeString) {
		return Bytes.toBytes(timeString);
	}

	public static byte[] toRowKey(Date time) {
		return toRowKey(rowFormatter.format(time));
	}
	
	public static Date toDate(byte[] rowkey) throws IllegalArgumentException {
		try {
			return rowFormatter.parse(Bytes.toString(rowkey));
		} catch (ParseException e) {
			e.printStackTrace();
			throw new IllegalArgumentException("row key \"" + Bytes.toString(rowkey) + 
					"\" does not match with (datetime) syntax \"" + fullRowFormat + "\"");
		}
	}

	public static byte[] getPrefix(byte[] row) {
		return Bytes.copy(row, 0, prefixFormat.length());
	}
	
	public static String getStringRow(byte[] row) {
		return Bytes.toString(row);
	}
	
	public static String getStringPrefix(byte[] row) {
		return getStringRow(row).substring(0, prefixFormat.length());
	}

//	public static byte[] calculatePrefix(byte[] row) throws ParseException {
//		Date prefixTime = prefixFormatter.parse(getStringPrefix(row));
//		Calendar cal = Calendar.getInstance();
//		cal.setTime(prefixTime);
//		cal.add(Calendar.DAY_OF_MONTH, -1);
//		return Bytes.toBytes(prefixFormatter.format(cal.getTime()));
//	}

}