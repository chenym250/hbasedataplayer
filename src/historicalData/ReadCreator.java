package historicalData;

import java.io.IOException;
import java.util.Date;
import historicalData.readers.DataReader;

public class ReadCreator {
	
	public static final int PER_MINUTE = 60;
	public static final int PER_SECOND = 1;
	
	public static DataReader create() throws IOException {
		return create(null, null, "", PER_MINUTE);
	}
	
	public static DataReader create(Date start, Date stop, String stock, int step) throws IOException {
		System.out.println("test");
		ConfigParser cp = new ConfigParser();
		cp.readSelectionConf();
		String[] dbTableName = cp.getTableName(start, stop, stock).split("\\.");
		String dbName = dbTableName[0];
		String tableName = dbTableName[1];
		DataReader dr = cp.getThisReader(dbName, PER_MINUTE);
		System.out.println(dr);
		dr.prepare(tableName, start, stop, 0, 0);
		return dr;
	}
	
}