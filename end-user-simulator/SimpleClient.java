import java.io.IOException;

import historicalData.ReadCreator;
import historicalData.readers.DataReader;


public class SimpleClient {
	
	public static void main(String[] args) throws IOException {
		DataReader dr = ReadCreator.create();
		dr.fetchOne();
		System.out.println("end");
	}
}